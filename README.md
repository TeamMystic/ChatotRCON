ChatotRCON
==========

This is a relatively simple `discord.py`-based bot intended for use with Minecraft's
RCON system. We recommend that you only use localhost RCON (and thus run the bot on
the same machine as your server), as RCON does not have any encryption.

Licensing
---------

ChatotRCON is licensed under the Artistic License 2.0. You may read up on the license
in the [LICENSE file](https://gitlab.com/TeamMystic/ChatotRCON/blob/master/LICENSE),
or [at choosealicense.com](http://choosealicense.com/licenses/artistic-2.0/).

Setup
-----

1. Install Python 3.5 or later
2. Install requirements - `python3.5 -m pip install -r requirements.txt`
3. Copy `config.yml.example` to `config.yml` and fill it out
4. `python3.5 -m app`

Usage
-----

Users sending messages in the channels you specify in `config.yml` with the
permissions you define will be able to use the following commands:

* `rcon <command> [args]` - Send a command to the server via RCON and output the response.

Multiple RCON connections
-------------------------

If you'd like to manage multiple RCON connections, simply run multiple instances of
this bot. You can easily have multiple bots logged into the same bot account, by
simply using the same token.

Note that if you do this and the instances have channels configured in common, they
will both act on commands and respond in that channel.
