# coding=utf-8
import logging

from aiowrap import wrap_sync

from app.safe_rcon import SafeRcon

from discord import Channel
from discord import Client
from discord import Member
from discord import Message
from discord import Object

from operator import attrgetter

__author__ = "Gareth Coles"


class Bot(Client):
    _prefix = None
    _has_printed_roles = False

    def __init__(self, config, *, loop=None, **options):
        super().__init__(loop=loop, **options)

        self._config = config

        self.logger = logging.getLogger("bot")
        self.rcon_logger = logging.getLogger("rcon")

    def ensure_prefix(self):
        if not self._prefix:
            self._prefix = self._config["discord"]["prefix"]

            if self._prefix == "@":
                self._prefix = "{} ".format(self.user.mention)

    def is_allowed(self, user: Member, channel: Channel):
        if self._config["discord"]["use_roles"]:
            roles = self._config["discord"]["roles"]

            for role in roles:
                has_role = False

                for user_role in user.roles:
                    if str(user_role.id) == role:
                        has_role = True

                if not has_role:
                    return False
            return True
        else:
            permissions = self._config["discord"]["permissions"]
            user_permissions = user.permissions_in(channel)

            for perm in permissions:
                if hasattr(user_permissions, perm):
                    if not getattr(user_permissions, perm)():
                        return False
                else:
                    self.logger.warn("Unknown permission: {}".format(perm))
                    return False
            return True

    def is_correct_server(self, server: str):
        return server == self._config["discord"]["server"]

    def is_correct_channel(self, channel: str):
        return channel in self._config["discord"]["channels"]

    def log_message(self, message: Message):
        for line in message.clean_content.split("\n"):
            self.logger.info(
                "{message.server.name} #{message.channel.name} / "
                "<{message.author.name}#{message.author.discriminator}> "
                "{line}".format(message=message, line=line)
            )

    @wrap_sync
    def send_rcon(self, command) -> bytes:
        self.logger.info("Connecting to rcon at {}:{}".format(
            self._config["rcon"]["server"],
            self._config["rcon"]["port"]
        ))

        client = SafeRcon(
            self._config["rcon"]["server"],
            self._config["rcon"]["port"],
            self._config["rcon"]["password"]
        )

        client.connect()
        result = client.rcon(command)
        client.disconnect()

        return result

    async def do_command(self, message, command, *args):
        command = command.lower()

        if hasattr(self, "command_{}".format(command)):
            command_callable = getattr(self, "command_{}".format(command))
            return await command_callable(message, command, *args)
        else:
            return await self.command_unknown(message, command, *args)

    async def send_to_admin_channel(self, message):
        await self.send_message(
            Object(self._config["discord"]["admin_channel"]), message
        )

    async def on_message(self, message: Message):
        if not message.server:
            return  # We don't handle DMs

        if message.author.id == self.user.id:
            return  # It me!

        if not self.is_correct_server(message.server.id):
            return  # Not a server we care about

        if not self._has_printed_roles:
            for role in sorted(message.server.roles, key=attrgetter("name")):
                self.logger.info("Role: {} -> {}".format(role.name, role.id))

            self._has_printed_roles = True

        if not self.is_correct_channel(message.channel.id):
            return  # Not a channel we care about

        self.ensure_prefix()
        self.log_message(message)

        if message.content.startswith(self._prefix):
            if not self.is_allowed(message.author, message.channel):
                return

            parts = message.content.lstrip(self._prefix).split(" ")

            if len(parts) > 1:
                command = parts[0]
                args = parts[1:]
            else:
                command = parts[0]
                args = []

            await self.do_command(message, command, *args)

    # region: Commands

    async def command_unknown(self, message, command, *args):
        self.logger.info("Unknown command: {}".format(command))

    async def command_rcon(self, message, command, *args):
        if len(args) < 1:
            self.send_message(
                message.channel,
                "{} Usage: `rcon <command> [args]`".format(message.user.mention)
            )

            return

        command = " ".join(args)

        try:
            data = await self.send_rcon(command)

            if data:
                data = data.strip()

            if data:
                await self.send_to_admin_channel(
                    "[RCON] `{}#{}`\n-> `{}`\n<- `{}`".format(
                        message.author.name, message.author.discriminator,
                        command, data
                    )
                )

                await self.send_message(
                    message.channel,
                    "{} ```{}```".format(message.author.mention, data)
                )
            else:
                await self.send_to_admin_channel(
                    "[RCON] `{}#{}`\n-> `{}`\n`Command sent; no response "
                    "data.`".format(
                        message.author.name, message.author.discriminator,
                        command
                    )
                )

                await self.send_message(
                    message.channel,
                    "{} Command sent; no response data.".format(
                        message.author.mention
                    )
                )
        except Exception as e:
            self.logger.exception("Failed to send data to RCON")

            await self.send_to_admin_channel(
                "[RCON] `{}#{}`\n-> `{}`\nFailed to send data: `{}`".format(
                    message.author.name, message.author.discriminator,
                    command, e
                )
            )

            await self.send_message(
                message.channel,
                "{} Failed to send data: `{}`".format(message.author.mention, e)
            )

    # endregion
